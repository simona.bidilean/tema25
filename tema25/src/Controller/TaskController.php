<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Task;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
class TaskController extends AbstractController
{
    /**
     * @Route("/task", name="task")
     */
 
    public function new(Request $request)
   {
       $task = new Task();

       $form = $this->createFormBuilder($task)
           ->add('task', TextType::class)
           ->add('dueDate', DateType::class)
           ->add('save', SubmitType::class, ['label' => 'Create Task'])
           ->getForm();
       $form->handleRequest($request);

       if ($form->isSubmitted() && $form->isValid()) {
           $task = $form->getData();

           $entityManager = $this->getDoctrine()->getManager();
           $entityManager->persist($task);
           $entityManager->flush();

           return new Response('Task created!');
       }

       return $this->render('task/new.html.twig', [
           'form' => $form->createView(),
       ]);
   }
}
