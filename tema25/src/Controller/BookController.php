<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Book;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class BookController extends AbstractController
{
    /**
     * @Route("/book", name="book")
     */
    public function new(Request $request){
    
        $book = new Book();
 
        $form = $this->createFormBuilder($book)
            ->add('Title', TextType::class)
            ->add('Pages', IntegerType::class)
            ->add('Genre', TextType::class)
            ->add('Author', TextType::class)
            ->add('save', SubmitType::class, ['label' => 'Create Book'])
            ->getForm();
        $form->handleRequest($request);
 
        if ($form->isSubmitted() && $form->isValid()) {
            $book = $form->getData();
 
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($book);
            $entityManager->flush();
 
            return new Response('Book created');
        }
 
        return $this->render('task/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
